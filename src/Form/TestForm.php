<?php
/**
 * @file
 * 父表单调用子表单演示
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\yunke_help\PluginForm;

/**
 * 该表单作为父表单 她会加载一个其他组件的子表单进来
 */
class TestForm extends FormBase
{

    public $plugin;

    public function __construct()
    {
        $this->plugin = new PluginForm(); //加载其他组件
    }

    public function getFormId()
    {
        return 'yunke_help_form_test_subform';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        //产生子表单
        $form['subform'] = [];
        $subformState = SubformState::createForSubform($form['subform'], $form, $form_state);
        $form['subform'] = $this->plugin->buildForm($form['subform'], $subformState);

        //父表单内容
        $form['phone_number'] = array(
            '#type'          => 'tel',
            '#title'         => '父表单的字段',
            '#default_value' => "13812345678",
        );
        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type'        => 'submit',
            '#value'       => "提交",
            '#button_type' => 'primary',
        );
        $form['actions']['reset'] = array(
            '#type'       => 'html_tag',
            '#tag'        => 'input',
            '#attributes' => ['type' => 'reset', 'value' => "重置", 'class' => "button"],
        );
        $form['#attributes']["target"] = "_blank";
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        //验证子表单
        $subformState = SubformState::createForSubform($form['subform'], $form, $form_state);
        $this->plugin->validateForm($form['subform'], $subformState);
        
        //验证附表单
        if (strlen($form_state->getValue('phone_number')) < 5) {
            $form_state->setErrorByName('phone_number', '父表单：电话号码太短');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        //提交子表单
        $subformState = SubformState::createForSubform($form['subform'], $form, $form_state);
        $this->plugin->submitForm($form['subform'], $subformState);

        //提交父表单
        drupal_set_message($this->t('父表单：电话为： @number', array('@number' => $form_state->getValue('phone_number'))));
    }
}
