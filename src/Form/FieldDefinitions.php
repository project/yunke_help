<?php

/**
 * 显示实体类型完整字段定义，含基本字段定义
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\FieldableEntityInterface;


class FieldDefinitions extends FormBase
{

    public function getFormId()
    {
        return 'yunke_help_Field_Definitions';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $entityTypeDefinitions = \Drupal::entityTypeManager()->getDefinitions();
        $entityTypeOptions = [];
        foreach ($entityTypeDefinitions as $id => $entityType) {
            if ($entityType->entityClassImplements(FieldableEntityInterface::class)) {
                $entityTypeOptions[$id] = $entityType->getLabel() . "($id)";
            }
        }

        $form['description'] = [
            '#markup' => '选择一个实体类型及其bundle，查看此bundle的完整字段定义（含bundle、基本字段定义及覆写），该功能仅限可字段化实体'
        ];
        $form['entityType'] = [
            '#type'         => 'select',
            //'#title'        => '选择实体类型：',
            '#options'      => $entityTypeOptions,
            '#empty_option' => $this->t('-select-'),
            '#required'     => TRUE,
            '#field_prefix' => '选择实体类型：',
            '#ajax'         => [
                'callback' => '::entityTypeSwitch',
                'wrapper'  => 'select-bundle-wrapper',
            ],
        ];

        $form['bundle'] = [
            '#type'         => 'select',
            '#empty_option' => $this->t('-select-'),
            '#required'     => TRUE,
            '#field_prefix' => '选择实体Bundle：',
            '#prefix'       => '<div id="select-bundle-wrapper">',
            '#suffix'       => '</div>',
            '#validated'    => true,
            //此处是因为采用了ajax，被选择的值并不在初始值中，默认验证不会通过，验证已经自定义，无需默认验证
        ];

        $form['actions'] = [
            '#type' => 'actions',
        ];
        $form['actions']['submit'] = array(
            '#type'  => 'submit',
            '#value' => '查看',
        );
        $form['actions']['reset'] = [
            '#type'        => 'button',
            '#button_type' => 'reset',
            '#value'       => $this->t('Reset'),
            '#attributes'  => [
                'onclick' => 'this.form.reset(); return false;',
            ],
        ];
        $form['#attributes']['target'] = "_blank";
        $form['#attached']['library'][] = 'yunke_help/removeFormSingleSubmit';
        $form['#title'] = "显示实体bundle完整字段定义";

        return $form;
    }

    /*
     * ajax方式返回内容
     */
    public function entityTypeSwitch($form, FormStateInterface $form_state)
    {
        $entityType = $form_state->getValue('entityType');
        $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entityType);
        $options = [];
        foreach ($bundles as $bundle => $info) {
            $options[$bundle] = $info['label'] . "({$bundle})";
        }
        $form['bundle']['#options'] = $options;
        return $form['bundle'];
    }

    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        $entityType = $form_state->getValue('entityType');
        if (empty(trim($entityType))) {
            $form_state->setErrorByName('entityType', "请选择一个可字段化实体类型");
        }
        $bundle = $form_state->getValue('bundle');
        $bundle = trim($bundle);
        if (empty($bundle)) {
            $form_state->setErrorByName('bundle', "请选择一个bundle");
        }
        $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entityType);
        if (!in_array($bundle, array_keys($bundles))) {
            $form_state->setErrorByName('bundle', "所选bundle不属于{$entityType}");
        }
    }

    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        $entityTypeId = $form_state->getValue('entityType');
        $bundle = $form_state->getValue('bundle');
        $entityFieldManager = \Drupal::service("entity_field.manager");
        $fieldDefinitions = $entityFieldManager->getFieldDefinitions($entityTypeId, $bundle);


        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
        echo "当前指定的实体类型id是：" . $entityTypeId . "，bundle是：{$bundle} \n\n";
        echo "该bundle有如下字段定义（含基本字段定义及覆写）：\n";
        print_r(array_keys($fieldDefinitions));
        echo "详细字段定义信息如下：\n";
        print_r($fieldDefinitions);
        echo "\n</pre>";
        die;
    }


}
