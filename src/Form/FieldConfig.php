<?php

/**
 * 查看储存的字段配置数据
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class FieldConfig extends FormBase
{

    public function getFormId()
    {
        return 'yunke_help_field_config';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $fields=\Drupal::entityTypeManager()->getStorage('field_config')->loadMultiple();
        foreach ($fields as $k=>$v){
            $fields[$k]=$k.'('.$v->label().')'; 
        }
        $form['#title'] = '查看字段配置数据';
        $form['field'] = [
            '#type'         => 'select',
            '#options'      => $fields,
            '#empty_option' => $this->t('-select-'),
            '#field_prefix' => '选择字段：',
            '#required' => TRUE,
        ];
        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type'        => 'submit',
            '#value'       => "提交",
            '#button_type' => 'primary',
        );
        $form['actions']['reset'] = array(
            '#type'       => 'html_tag',
            '#tag'        => 'input',
            '#attributes' => ['type' => 'reset', 'value' => "重置", 'class' => "button"],
        );
        $form['#attributes']["target"] = "_blank";
        $form['#attached']['library'][] = 'yunke_help/removeFormSingleSubmit';
        return $form;
    }

    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        $field = $form_state->getValue('field');
        if (empty(trim($field))) {
            $form_state->setErrorByName('field', "字段不能为空");
        }
    }

    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        $field= $form_state->getValue('field');
        $field_storage=explode('.', $field);
        $field_storage=$field_storage[0].'.'.$field_storage[2];
        $field_config=\Drupal::entityTypeManager()->getStorage('field_config')->load($field)->toArray();
        $field_storage_config=\Drupal::entityTypeManager()->getStorage('field_storage_config')->load($field_storage)->toArray();
        
        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
        echo "当前指定的字段是：" . $field . "\n\n";
        echo "字段配置数据如下：\n";
        print_r($field_config);
        echo "\n\n字段储存配置数据如下：\n";
        print_r($field_storage_config);
        echo "\n</pre>";
        die;
    }

}
