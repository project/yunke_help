<?php
/**
 *  by:yunke
 *  email:phpworld@qq.com
 *  time:20180610
 */

namespace Drupal\yunke_help\Controller;

use Drupal\Core\Site\Settings;

class DataDumper
{
    /**
     * 系统根目录的绝对路径 不带后缀斜杠 如：C:\root\drupal
     *
     * @var string
     */
    protected $root;

    /**
     * 本模块相对于系统根目录的路径，不带前后缀斜杠
     *
     * @var string
     */
    protected $yunke_help_path;


    public function __construct()
    {
        $this->root = \Drupal::service("app.root");
        $this->yunke_help_path = \Drupal::moduleHandler()->getModule("yunke_help")->getPath();
    }

    /**
     * 打印运行时容器中的数据，包含运行时动态定义的数据
     */
    public function showRunContainer()
    {
        $container = \Drupal::getContainer();
        $ref = new \ReflectionObject($container);

        $parameters = $ref->getProperty('parameters');
        $parameters->setAccessible(true);
        $parameters = $parameters->getValue($container);

        $aliases = $ref->getProperty('aliases');
        $aliases->setAccessible(true);
        $aliases = $aliases->getValue($container);

        $serviceDefinitions = $ref->getProperty('serviceDefinitions');
        $serviceDefinitions->setAccessible(true);
        $serviceDefinitions = $serviceDefinitions->getValue($container);
        foreach ($serviceDefinitions as $key => $value) {
            $serviceDefinitions[$key] = unserialize($value);
        }

        $services = $ref->getProperty('services');
        $services->setAccessible(true);
        $services = $services->getValue($container);
        $services = array_keys($services);
        $synthetic = [];
        foreach ($services as $value) {
            if (!array_key_exists($value, $serviceDefinitions)) {
                $synthetic[] = $value;
            }
        }

        $syntheticDefinitions = [];
        foreach ($serviceDefinitions as $key => $value) {
            if (!empty($value["synthetic"])) {
                $syntheticDefinitions[] = $key;
            }
        }

        $privateServices = $ref->getProperty('privateServices');
        $privateServices->setAccessible(true);
        $privateServices = $privateServices->getValue($container);


        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
        echo "以下是运行时容器数据（包含运行时设置的数据）：\n\n";
        echo "服务别名：\n";
        print_r($aliases);
        echo "\n\n";

        echo "容器参数：\n";
        print_r($parameters);
        echo "\n\n";

        echo "运行时注入的服务：\n";
        print_r($synthetic);
        echo "\n\n";

        echo "有定义的合成服务：\n";
        print_r($syntheticDefinitions);
        echo "\n\n";

        echo "私有服务名：\n";
        print_r(array_keys($privateServices));
        echo "\n\n";

        echo "共享服务定义：\n";
        print_r($serviceDefinitions);
        echo "\n\n";


        echo "</pre>";
        die;
    }

    /**
     * 打印被缓存的容器定义数据，不包括动态注册的服务等
     */
    public function showContainer()
    {
        $parts = ['service_container', 'prod', \Drupal::VERSION, Settings::get('deployment_identifier'), PHP_OS, serialize(Settings::get('container_yamls'))];
        $cacheKey = implode(':', $parts);


        $cache = \Drupal::service("cache_factory")->get("container")->get($cacheKey);

        if (!$cache) {
            echo "系统尚未编译容器，请先访问任意页面，再试";
            die;
        }
        $containerDefinition = $cache->data;
        foreach ($containerDefinition["services"] as $key => $value) {
            $containerDefinition["services"][$key] = unserialize($value);
        }
        $this->showData("容器定义缓存数据", $containerDefinition);
        die;
    }

    /**
     * 打印运行时事件派发器中的数据，以便知道事件订阅情况
     * 辅助理解系统执行流程
     */
    public function showEvent()
    {
        $event_dispatcher = \Drupal::service("event_dispatcher");
        $ref = new \ReflectionObject($event_dispatcher);
        $listeners = $ref->getProperty('listeners');
        $listeners->setAccessible(true);
        $listeners = $listeners->getValue($event_dispatcher);

        $unsorted = $ref->getProperty('unsorted');
        $unsorted->setAccessible(true);
        $unsorted = $unsorted->getValue($event_dispatcher);
        if (!empty($unsorted)) {
            foreach (array_keys($unsorted) as $event_name) {
                if (isset($listeners[$event_name])) {
                    krsort($listeners[$event_name]);
                }
            }
        }
        foreach ($listeners as $event_name => &$event_definitions) {
            foreach ($event_definitions as $priority => &$definitions) {
                foreach ($definitions as $key => &$definition) {
                    if (!isset($definition['service'])) {
                        if (is_array($definition['callable'])) {
                            if (is_string($definition['callable'][0])) {
                                $definition['service'] = $definition['callable'];
                            } elseif (is_object($definition['callable'][0])) {
                                $obj = new \ReflectionObject($definition['callable'][0]);
                                $definition['service'] = [$obj->getName(), $definition['callable'][1]];
                            }
                        } else {
                            $definition['service'] = $definition['callable'];
                        }
                    }
                    if (isset($definition['callable'])) {
                        unset($definition['callable']);
                    }
                }
            }
        }
        $this->showData("运行时事件派发器中的事件订阅数据", $listeners, true);
        exit();

    }

    /**
     * 打印主题钩子注册数据
     */
    public function showThemeRegistry()
    {
        $registry = \Drupal::service("theme.registry")->get();
        $this->showData("主题钩子注册数据", $registry, true);
        exit();

    }

    protected function showData($name = null, $data = array(), $showKey = false)
    {
        echo "<pre>";
        echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
        echo "以下是" . $name . "：\n";
        if ($showKey) {
            print_r(array_keys($data));
        }
        print_r($data);
        echo "</pre>";
    }

}

