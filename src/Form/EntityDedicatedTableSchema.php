<?php

/**
 * 显示实体类型专用表的数据库Schema
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\FieldableEntityInterface;


class EntityDedicatedTableSchema extends FormBase
{

    public function getFormId()
    {
        return 'yunke_help_Entity_Dedicated_Table_Schema';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $entityTypeDefinitions = \Drupal::entityTypeManager()->getDefinitions();
        $entityTypeOptions = [];
        foreach ($entityTypeDefinitions as $id => $entityType) {
            if ($entityType->entityClassImplements(FieldableEntityInterface::class)) {
                $entityTypeOptions[$id] = $entityType->getLabel() . "($id)";
            }
        }
        foreach (array_keys($entityTypeOptions) as $id){
            $storage=\Drupal::entityTypeManager()->getStorage($id);
            if(!($storage instanceof \Drupal\Core\Entity\Sql\SqlEntityStorageInterface)){
                unset($entityTypeOptions[$id]);
            }
        }
        

        $form['description'] = [
            '#markup' => '选择一个实体类型及使用专用表的字段，查看专用表Schema，该功能仅限可字段化实体'
        ];
        $form['entityType'] = [
            '#type'         => 'select',
            //'#title'        => '选择实体类型：',
            '#options'      => $entityTypeOptions,
            '#empty_option' => $this->t('-select-'),
            '#required'     => TRUE,
            '#field_prefix' => '选择实体类型：',
            '#ajax'         => [
                'callback' => '::entityTypeSwitch',
                'wrapper'  => 'select-dedicate-schema-wrapper',
            ],
        ];

        $form['field'] = [
            '#type'         => 'select',
            '#empty_option' => $this->t('-select-'),
            '#required'     => TRUE,
            '#field_prefix' => '选择专用表字段：',
            '#prefix'       => '<div id="select-dedicate-schema-wrapper">',
            '#suffix'       => '</div>',
            '#validated'    => true,
            //此处是因为采用了ajax，被选择的值并不在初始值中，默认验证不会通过，验证已经自定义，无需默认验证
        ];

        $form['actions'] = [
            '#type' => 'actions',
        ];
        $form['actions']['submit'] = array(
            '#type'  => 'submit',
            '#value' => '查看',
        );
        $form['actions']['reset'] = [
            '#type'        => 'button',
            '#button_type' => 'reset',
            '#value'       => $this->t('Reset'),
            '#attributes'  => [
                'onclick' => 'this.form.reset(); return false;',
            ],
        ];
        $form['#attributes']['target'] = "_blank";
        $form['#attached']['library'][] = 'yunke_help/removeFormSingleSubmit';
        $form['#title'] = "显示实体专用表的数据库schema";

        return $form;
    }

    /*
     * 依据实体类型id以ajax方式返回bundle选项内容
     */
    public function entityTypeSwitch(&$form, FormStateInterface $form_state)
    {
        $entityTypeId = $form_state->getValue('entityType');
        $entityFieldManager = \Drupal::service("entity_field.manager");
        $fieldStorageDefinitions = $entityFieldManager->getFieldStorageDefinitions($entityTypeId);
        $table_mapping = \Drupal::entityTypeManager()->getStorage($entityTypeId)->getTableMapping($fieldStorageDefinitions);
        $options = [];
        foreach ($fieldStorageDefinitions as $field => $field_storage_definition) {
            if ($table_mapping->requiresDedicatedTableStorage($field_storage_definition)) {
                $options[$field] = $field;
            }
        }
        if (count($options)) {
            $form['field']['#options'] = $options;
        } else {
            $form['field']['#options'] = ['该实体类型没有专用表字段'];
        }
        return $form['field'];
    }

    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        $entityType = $form_state->getValue('entityType');
        if (empty(trim($entityType))) {
            $form_state->setErrorByName('entityType', "请选择一个可字段化实体类型");
        }
        $field = $form_state->getValue('field');
        $field = trim($field);
        if (empty($field)) {
            $form_state->setErrorByName('field', "请选择一个使用专用表的字段");
        }
        $entityFieldManager = \Drupal::service("entity_field.manager");
        $fieldStorageDefinitions = $entityFieldManager->getFieldStorageDefinitions($entityType);
        $table_mapping = \Drupal::entityTypeManager()->getStorage($entityType)->getTableMapping($fieldStorageDefinitions);
        $options = [];
        foreach ($fieldStorageDefinitions as $field => $field_storage_definition) {
            if ($table_mapping->requiresDedicatedTableStorage($field_storage_definition)) {
                $options[$field] = $field;
            }
        }
        if (!in_array($field, array_keys($options))) {
            $form_state->setErrorByName('field', "所选字段不属于{$entityType}的专用表字段");
        }
    }

    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        $entityTypeId = $form_state->getValue('entityType');
        $fieldStorageDefinitions = \Drupal::service("entity_field.manager")->getFieldStorageDefinitions($entityTypeId);
        $field = $form_state->getValue('field');
        $fieldStorageDefinition = $fieldStorageDefinitions[$field];
        $storage = \Drupal::entityTypeManager()->getStorage($entityTypeId);
        $refStorage = new \ReflectionObject($storage);
        $methodName = 'getStorageSchema';
        if (!$refStorage->hasMethod($methodName)) {
            echo '该实体类型的储存处理器没有继承系统默认的储存处理器（Drupal\Core\Entity\Sql\SqlContentEntityStorage），不能处理';
            die;
        }
        $reflectionMethod_getStorageSchema = $refStorage->getMethod($methodName);
        $reflectionMethod_getStorageSchema->setAccessible(true);
        $storageSchema = $reflectionMethod_getStorageSchema->invoke($storage);
        $refStorageSchema = new \ReflectionObject($storageSchema);
        $reflectionMethod_getDedicatedTableSchema = $refStorageSchema->getMethod('getDedicatedTableSchema');
        $reflectionMethod_getDedicatedTableSchema->setAccessible(true);
        $schema = $reflectionMethod_getDedicatedTableSchema->invokeArgs($storageSchema, [$fieldStorageDefinition]);

        /*
         * 额外提示：有了这个$schema后，可以通过以下代码创建实体类型的专用数据库表
        $schema_handler = \Drupal::database()->schema();
        foreach ($schema as $table_name => $table_schema) {
            if (!$schema_handler->tableExists($table_name)) {
                $schema_handler->createTable($table_name, $table_schema);
            }
        }
        */

        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
        echo "当前指定的实体类型id是：" . $entityTypeId . " 专用表字段是：{$field}\n\n";
        echo "有如下专用表：\n";
        print_r(array_keys($schema));
        echo "各专用表schema定义信息如下（该数组可以直接用于数据库组件建表，示例见本程序源代码）：\n";
        print_r($schema);
        echo "\n</pre>";
        die;
    }


}
