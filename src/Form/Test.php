<?php

/**
 * 用于表单测试
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class Test extends FormBase
{

    public function getFormId()
    {
        return 'yunke_help_test_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        
        //演示作用
        $form['a']['#tree'] = true;
        $form['a']['b']['c'] = array(
            '#type'  => 'textfield',
            '#title' => "嵌套值测试", '#default_value' => 55,
            '#size'  => 100,
        );
        $form['a']['b']['x'] = array(
            '#tree'  => false,
            '#type'  => 'textfield',
            '#title' => "嵌套值测试2",
            '#size'  => 100,
        );

        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type'        => 'submit',
            '#value'       => "提交",
            '#button_type' => 'primary',
        );
        $form['actions']['reset'] = array(
            '#type'       => 'html_tag',
            '#tag'        => 'input',
            '#attributes' => ['type' => 'reset', 'value' => "重置", 'class' => "button"],
        );
        $form['#attributes']["target"] = "_blank";
        $form['#attached']['library'][] = 'yunke_help/removeFormSingleSubmit';
        return $form;
    }

    public function validateForm(array & $form, FormStateInterface $form_state)
    {

        $value = $form_state->getValue(['a', 'b', 'c']);
        if (empty(trim($value))) {
            $form_state->setErrorByName(implode('][', ['a', 'b', 'c']), "值不能为空");
        }

    }

    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        $form_state->cleanValues();
        $cleanValues = $form_state->getValues();

        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
        echo "提交的值是：\n\n";
        print_r($values);
        echo "\n清理后的值是：\n\n";
        print_r($cleanValues);
        echo "\n</pre>";
        die;
    }

}
