<?php

/**
 * 构建插件管理器表单
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class PluginManager extends FormBase
{

    public function getFormId()
    {
        return 'yunke_help_pluginManager';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $container = \Drupal::getContainer();
        $serviceIds = $container->getServiceIds();
        $pluginManagerIds = [];
        foreach ($serviceIds as $id) {
            if (stripos($id, 'plugin.manager.') === 0) {
                $pluginManagerIds[$id] = $id;
            }
        }

        $form['#attributes']['class'][] = 'yk-table__item';
        $form['#action'] = Url::fromRoute('yunke_help.plugin')->toString();
        //$form['#method'] = 'get'; //如需get提交使用$form_state->setMethod("get");即可
        $form['#attributes']['target'] = "_blank";
        //$form['#theme']="yunke_help_plugin";

        $form['pluginManagerId'] = [
            '#type'         => 'select',
            //'#title'        => '选择插件管理器ID：',
            '#options'      => $pluginManagerIds,
            '#empty_option' => $this->t('-select-'),
            '#field_prefix' => '选择插件管理器ID：',
        ];
        $form['actions'] = [
            '#type' => 'actions',
        ];
        $form['actions']['clearCache'] = array(
            '#type'   => 'submit',
            '#value'  => '清理定义缓存',
            '#submit' => array('::clearCache'),
        );
        $form['actions']['showPlugins'] = array(
            '#type'   => 'submit',
            '#value'  => '查看插件数据',
            '#submit' => array('::showPlugins'),
        );
        $form['actions']['reset'] = [
            '#type'        => 'button',
            '#button_type' => 'reset',
            '#value'       => $this->t('Reset'),
            '#attributes'  => [
                'onclick' => 'this.form.reset(); return false;',
            ],
        ];

        return $form;
    }

    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        $pluginManagerId = $form_state->getValue('pluginManagerId');
        if (empty(trim($pluginManagerId))) {
            $form_state->setErrorByName('pluginManagerId', "请选择一个插件管理器类型");
        }
    }

    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        //不使用默认的提交处理器
    }

    public function clearCache(array & $form, FormStateInterface $form_state)
    {
        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
        $pluginManagerId = $form_state->getValue('pluginManagerId');
        $container = \Drupal::getContainer();
        if ($container->has($pluginManagerId)) {
            $pluginManager = \Drupal::service($pluginManagerId);
        } else {
            echo "错误";
            return;
        }
        if ($pluginManager instanceof \Drupal\Component\Plugin\Discovery\CachedDiscoveryInterface) {
            $pluginManager->clearCachedDefinitions();
            echo "插件管理器：" . $pluginManagerId . "的插件定义缓存已被清理";
        } else {
            echo "插件管理器：" . $pluginManagerId . "没有实现可缓存发现接口，插件定义无缓存或需通过重建刷新";
        }

        echo "\n</pre>";
        die;
    }

    public function showPlugins(array & $form, FormStateInterface $form_state)
    {
        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
        $pluginManagerId = $form_state->getValue('pluginManagerId');
        $container = \Drupal::getContainer();
        if ($container->has($pluginManagerId)) {
            $pluginManager = \Drupal::service($pluginManagerId);
        } else {
            echo "错误";
            return;
        }
        $definitions = $pluginManager->getDefinitions();
        echo "插件管理器：" . $pluginManagerId . "有以下插件：\n";
        print_r(array_keys($definitions));
        echo "\n插件定义如下：\n";
        print_r($definitions); //部分定义是使用对象方式  打印时可能存在内存超限 需要改进
        echo "\n</pre>";
        die;
    }

}
