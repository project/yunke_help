<?php
/**
 *  by:yunke
 *  email:phpworld@qq.com
 *  time:20180712
 */

namespace Drupal\yunke_help\Controller;


/**
 * Class DataPrint
 *
 * @package Drupal\yunke_help\Controller
 */
class DataPrint
{
  /**
   * 系统根目录的绝对路径 不带后缀斜杠 如：C:\root\drupal
   *
   * @var string
   */
  protected $root;

  /**
   * 本模块相对于系统根目录的路径，不带前后缀斜杠
   *
   * @var string
   */
  protected $yunke_help_path;


  public function __construct()
  {
    //$this->root = \Drupal::service("app.root");
    //$this->yunke_help_path = \Drupal::moduleHandler()->getModule("yunke_help")->getPath();
  }

  /**
   * 中转数据查看操作
   */
  public function index($type = null)
  {
    if (empty($type)) {
      echo "链接不正确";
      die;
    }
    switch ($type) {
      case "config-schema":
        $this->configSchema();
        break;
      case "cache-contexts":
        $this->cacheContexts();
        break;
      case "data-type":
        $this->data_type();
        break;
      case "blockEntity-activeTheme":
        $this->blocksEntityActiveTheme();
        break;
      case "route-enhancers":
        $this->routeEnhancer();
        break;
      case "paramconverter":
        $this->paramconverter();
        break;
      case "language-list":
        $this->languageList();
        break;
      case "bundle-info":
        $this->bundleInfo();
        break;
      case "field-map":
        $this->fieldMap();
        break;
      case "language-negotiation-config":
        $this->languageNegotiationConfig();
        break;
      case "route-name":
        return $this->routeByName();
      case "hook":
        return $this->hook();
      case "menu-tree":
        return $this->menuTree();
      case "route-path":
        return $this->routeByPath();
      case "base-field-definitions":
        return $this->baseFieldDefinitions();
      case "bundle-field-definitions":
        return $this->bundleFieldDefinitions();
      case "field-definitions":
        return $this->fieldDefinitions();
      case "field-storage-definitions":
        return $this->fieldStorageDefinitions();
      case "extra-fields":
        return $this->extraFields();
      case "entity-storage-schema":
        return $this->entitySchema();
      case "entity-Dedicate-schema":
        return $this->entityDedicatedTableSchema();
      case "language-config-override":
        return $this->languageConfigOverride();
      case "field-config":
        return $this->fieldConfig();
      case "view-config":
        return $this->viewConfig();
      case "schema-version":
        $this->schemaVersion();
        break;
      case "post-update":
        $this->postUpdate();
        break;
      default:
        echo "链接不正确";
    }
    die;

  }

  /**
   * 显示全部已安装模块执行过的数据更新钩子，其用于追踪数据更新
   */
  public function postUpdate(){
    $postUpdates = \Drupal::keyValue('post_update')->get('existing_updates', []);
    $this->showData("模块已执行过的数据更新钩子", $postUpdates);
  }

  /**
   * 显示全部已安装模块的schema版本号，其用于追踪更新
   */
  public function schemaVersion(){
    $schema = \Drupal::keyValue('system.schema')->getAll();;
    $this->showData("已安装模块的schema版本号", $schema);
  }

  /**
   * 显示视图数据
   */
  public function viewConfig()
  {
    return \Drupal::formBuilder()->getForm("\Drupal\yunke_help\Form\ViewConfig");
  }

  /**
   * 显示字段数据
   */
  public function fieldConfig()
  {
    return \Drupal::formBuilder()->getForm("\Drupal\yunke_help\Form\FieldConfig");
  }

  /**
   * 显示语言配置覆写数据
   */
  public function languageConfigOverride()
  {
    return \Drupal::formBuilder()->getForm("\Drupal\yunke_help\Form\LanguageConfigOverride");
  }

  /**
   * 显示实体专用表数据库Schema
   */
  public function entityDedicatedTableSchema()
  {
    return \Drupal::formBuilder()->getForm("\Drupal\yunke_help\Form\EntityDedicatedTableSchema");
  }

  /**
   * 显示实体类型的数据库共享表Schema
   */
  public function entitySchema()
  {
    return \Drupal::formBuilder()->getForm("\Drupal\yunke_help\Form\EntitySchema");
  }

  /**
   * 查看bundle伪字段定义
   */
  public function extraFields()
  {
    return \Drupal::formBuilder()->getForm("\Drupal\yunke_help\Form\ExtraFields");
  }

  /**
   * 查看字段映射信息
   */
  public function fieldMap()
  {
    $entityFieldManager = \Drupal::service("entity_field.manager");
    $fieldMap = $entityFieldManager->getFieldMap();

    echo "<pre>\n";
    echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
    echo "有如下实体类型的字段映射：\n";
    print_r(array_keys($fieldMap));
    echo "字段映射信息如下：\n";
    print_r($fieldMap);
    echo "\n</pre>";
  }

  /**
   * 查看语言协商配置
   */
  public function languageNegotiationConfig()
  {
    $languageTypes = \Drupal::configFactory()->get('language.types')->get();
    $this->showData("语言协商配置信息", $languageTypes);
  }

  /**
   * 查看实体类型的储存定义
   */
  public function fieldStorageDefinitions()
  {
    return \Drupal::formBuilder()->getForm("\Drupal\yunke_help\Form\FieldStorageDefinitions");
  }

  /**
   * 查看实体bundle完整字段定义
   */
  public function fieldDefinitions()
  {
    return \Drupal::formBuilder()->getForm("\Drupal\yunke_help\Form\FieldDefinitions");
  }

  /**
   * 查看实体类型的bundle特有字段定义
   */
  public function bundleFieldDefinitions()
  {
    return \Drupal::formBuilder()->getForm("\Drupal\yunke_help\Form\BundleFieldDefinitions");
  }

  /**
   * 查看实体类型的基本字段定义
   */
  public function baseFieldDefinitions()
  {
    return \Drupal::formBuilder()->getForm("\Drupal\yunke_help\Form\BaseFieldDefinitions");
  }

  /**
   * 查看全部实体类型的bundle信息
   */
  public function bundleInfo()
  {
    \Drupal::service('entity_type.bundle.info')->clearCachedBundles();
    $bundle_info = \Drupal::service('entity_type.bundle.info')->getAllBundleInfo();
    $this->showData('全部实体类型的bundle信息', $bundle_info, true);
  }

  /**
   * 显示语言代码列表
   */
  public function languageList()
  {
    $languageList = \Drupal\Core\Language\LanguageManager::getStandardLanguageList();
    $this->showData('标准语言代码列表', $languageList, true);
  }

  /**
   * 显示菜单树结构
   */
  public function menuTree()
  {
    return \Drupal::formBuilder()->getForm("\Drupal\yunke_help\Form\MenuTree");
  }

  /**
   * 显示所有参数转化器服务id
   */
  public function paramconverter()
  {
    $paramconverter_manager = \Drupal::service("paramconverter_manager");
    $ref = new \ReflectionObject($paramconverter_manager);

    $converters = $ref->getProperty('converters');
    $converters->setAccessible(true);
    $convertersArr = $converters->getValue($paramconverter_manager);
    $serviceIDs = array_keys($convertersArr);
    /*
    foreach ($convertersArr as $converter) {
        $serviceIDs[] = $converter->_serviceId;
    }
    */
    $this->showData("系统中所有启用的参数转换器服务ID", $serviceIDs);
    die;
  }

  /**
   * 依据路由名查看路由定义
   */
  public function routeByName()
  {
    return \Drupal::formBuilder()->getForm("\Drupal\yunke_help\Form\RouteByName");
  }

  /**
   * 查看钩子实现
   */
  public function hook()
  {
    return \Drupal::formBuilder()->getForm("\Drupal\yunke_help\Form\Hook");
  }

  /**
   * 依据url路径查看路由处理结果
   */
  public function routeByPath()
  {
    return \Drupal::formBuilder()->getForm("\Drupal\yunke_help\Form\RouteByPath");
  }

  /**
   * 显示当前主题的块配置实体信息
   */
  public function blocksEntityActiveTheme()
  {
    $activeTheme = \Drupal::theme()->getActiveTheme();
    $entities = \Drupal::entityTypeManager()->getStorage("block")->loadByProperties(['theme' => $activeTheme->getName()]);
    echo "<pre>";
    echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
    echo "当前主题在用的块配置实体数量：" . count($entities) . "个\n";
    echo "配置id如下：\n";
    print_r(array_keys($entities));
    echo "\n配置数据如下：\n";
    print_r($entities);
    echo "\n</pre>";
    exit();
  }


  /**
   * 打印数据的类型
   */
  public function data_type()
  {
    $typed_data_manager = \Drupal::service("typed_data_manager");
    $definitions = $typed_data_manager->getDefinitions();
    $this->showData("数据类型", $definitions, true);
    exit();
  }


  /**
   * 打印缓存上下文信息
   */
  public function cacheContexts()
  {
    $cache_contexts_manager = \Drupal::service("cache_contexts_manager");
    $definitions = $cache_contexts_manager->getAll();
    $definitionsWithLabel = $cache_contexts_manager->getLabels(true);
    foreach ($definitionsWithLabel as $key => $value) {
      $definitionsWithLabel[$key] = (string)$value;
    }
    echo "<pre>";
    echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
    echo "系统中有以下缓存上下文\n";
    print_r($definitions);
    echo "\n各缓存上下文名称：\n";
    print_r($definitionsWithLabel);
    echo "</pre>";
    exit();
  }

  /**
   * 查看配置模式定义数据
   */
  public function configSchema()
  {
    $configSchema = \Drupal::service("config.typed");
    $definitions = $configSchema->getDefinitions();
    $this->showData("config schema", $definitions, true);
    exit();
  }

  /**
   * 显示系统中的所有路由增强器服务id
   */
  public function routeEnhancer()
  {
    $router = \Drupal::service("router.no_access_checks");
    $ref = new \ReflectionObject($router);

    $enhancers = $ref->getProperty('enhancers');
    $enhancers->setAccessible(true);
    $enhancersObj = $enhancers->getValue($router);
    $serviceIDs = [];
    foreach ($enhancersObj as $enhancer) {
      $serviceIDs[] = $enhancer->_serviceId;
    }
    $this->showData("系统中所有启用的路由增强器服务ID", $serviceIDs);
    die;

  }

  /**
   * @param null  $name    打印数据的名称
   * @param array $data    数据内容
   * @param bool  $showKey 是否显示键名
   */
  protected function showData($name = null, $data = array(), $showKey = false)
  {
    echo "<pre>";
    echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
    echo "以下是" . $name . "数据：\n";
    if ($showKey) {
      $dataKey = array_keys($data);
      echo "共有 " . count($dataKey) . " 项:\n";
      print_r($dataKey);
    }
    echo "\n数据如下：\n";
    print_r($data);
    echo "</pre>";
  }


}

