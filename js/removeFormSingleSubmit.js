//由于在页面加载core/drupal.form库时，表单post提交只能进行一次，本文件用于移除该特性，可多次提交，使用本文件的库应该依赖于core/drupal.form
if(Drupal.behaviors.formSingleSubmit.attach){
    Drupal.behaviors.formSingleSubmit.attach=function () {};
}