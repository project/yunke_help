<?php

namespace Drupal\yunke_help\Plugin\EntityReferenceSelection;

use Drupal\node\NodeInterface;
use Drupal\node\Plugin\EntityReferenceSelection\NodeSelection;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;

/**
 * 依据任意节点字段进行选择
 *
 * @EntityReferenceSelection(
 *   id = "yunke_help:node",
 *   label = @Translation("yunke_help any node field selection"),
 *   entity_types = {"node"},
 *   group = "yunke_help",
 *   base_plugin_label = @Translation("yunke_help"),
 *   weight = 1
 * )
 */
class NodeAnyFieldSelection extends NodeSelection
{

    public function defaultConfiguration()
    {
        return [
                'target_field'   => 'title',
                'match_operator' => 'CONTAINS',
            ] + parent::defaultConfiguration();
    }

    /**
     * {@inheritdoc}
     * 不允许新建实体，输入值可能是非标题字段值，无法确保节点标题存在
     */
    public function setConfiguration(array $configuration)
    {
        parent::setConfiguration($configuration);
        $this->configuration['auto_create'] = FALSE;
    }

    /**
     * {@inheritdoc}
     * 重构配置表单，去掉新建实体配置，添加按字段选择，并可指定操作符
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {
        $configuration = $this->getConfiguration();
        $entity_type_id = $configuration['target_type'];
        $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
        $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);


        $bundle_options = [];
        foreach ($bundles as $bundle_name => $bundle_info) {
            $bundle_options[$bundle_name] = $bundle_info['label'];
        }
        natsort($bundle_options);

        $form['target_bundles'] = [
            '#type'                    => 'checkboxes',
            '#title'                   => $entity_type->getBundleLabel(),
            '#options'                 => $bundle_options,
            '#default_value'           => (array)$configuration['target_bundles'],
            '#required'                => TRUE,
            '#size'                    => 6,
            '#multiple'                => TRUE,
            '#element_validate'        => [[get_class($this), 'elementValidateFilter']],
            '#ajax'                    => TRUE,
            '#limit_validation_errors' => [],
        ];

        $form['target_bundles_update'] = [
            '#type'                    => 'submit',
            '#value'                   => $this->t('Update form'),
            '#limit_validation_errors' => [],
            '#attributes'              => [
                'class' => ['js-hide'],
            ],
            '#submit'                  => [[EntityReferenceItem::class, 'settingsAjaxSubmit']],
        ];


        $fields = [];
        foreach (array_keys($bundles) as $bundle) {
            $bundle_fields = array_filter($this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle), function ($field_definition) {
                return !$field_definition->isComputed();
            });
            foreach ($bundle_fields as $field_name => $field_definition) {
                /* @var \Drupal\Core\Field\FieldDefinitionInterface $field_definition */
                $columns = $field_definition->getFieldStorageDefinition()->getColumns();
                // If there is more than one column, display them all, otherwise just
                // display the field label.
                // @todo: Use property labels instead of the column name.
                if (count($columns) > 1) {
                    foreach ($columns as $column_name => $column_info) {
                        $fields[$field_name . '.' . $column_name] = $this->t('@label (@column)', ['@label' => $field_definition->getLabel(), '@column' => $column_name]);
                    }
                } else {
                    $fields[$field_name] = $this->t('@label', ['@label' => $field_definition->getLabel()]);
                }
            }
        }

        $form['target_field'] = [
            '#type'                    => 'select',
            '#title'                   => $this->t('field by'), //依据设定的字段进行选择
            '#options'                 => $fields,
            '#required'                => TRUE,
            '#ajax'                    => TRUE,
            '#limit_validation_errors' => [],
            '#default_value'           => $configuration['target_field'],
        ];
        $match_operator = [
            'CONTAINS'    => 'CONTAINS',
            'STARTS_WITH' => 'STARTS_WITH',
            'ENDS_WITH'   => 'ENDS_WITH',
            '>'           => '>',
            '>='          => '>=',
            '<'           => '<',
            '<='          => '<=',
            '='           => '=',
            '<>'          => '<>',
            'IS NOT NULL' => 'IS NOT NULL',
            'IS NULL'     => 'IS NULL',
        ];
        $form['match_operator'] = [
            '#type'                    => 'select',
            '#title'                   => $this->t('match operator'),
            '#options'                 => $match_operator,
            '#required'                => TRUE,
            '#ajax'                    => TRUE,
            '#limit_validation_errors' => [],
            '#default_value'           => $configuration['match_operator'],
        ];

        $form['sort']['field'] = [
            '#type'                    => 'select',
            '#title'                   => $this->t('Sort by'),
            '#options'                 => [
                    '_none' => $this->t('- None -'),
                ] + $fields,
            '#ajax'                    => TRUE,
            '#limit_validation_errors' => [],
            '#default_value'           => $configuration['sort']['field'],
        ];

        $form['sort']['settings'] = [
            '#type'       => 'container',
            '#attributes' => ['class' => ['entity_reference-settings']],
            '#process'    => [[EntityReferenceItem::class, 'formProcessMergeParent']],
        ];

        if ($configuration['sort']['field'] != '_none') {
            $form['sort']['settings']['direction'] = [
                '#type'          => 'select',
                '#title'         => $this->t('Sort direction'),
                '#required'      => TRUE,
                '#options'       => [
                    'ASC'  => $this->t('Ascending'),
                    'DESC' => $this->t('Descending'),
                ],
                '#default_value' => $configuration['sort']['direction'],
            ];
        }

        $form['auto_create'] = [ //禁止新建实体
            '#type'  => 'value',
            '#value' => $configuration['auto_create'],
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS')
    {
        $configuration = $this->getConfiguration();
        $target_type = $configuration['target_type'];
        $entity_type = $this->entityTypeManager->getDefinition($target_type);

        $query = $this->entityTypeManager->getStorage($target_type)->getQuery();

        // If 'target_bundles' is NULL, all bundles are referenceable, no further
        // conditions are needed.
        if (is_array($configuration['target_bundles'])) {
            // If 'target_bundles' is an empty array, no bundle is referenceable,
            // force the query to never return anything and bail out early.
            if ($configuration['target_bundles'] === []) {
                $query->condition($entity_type->getKey('id'), NULL, '=');
                return $query;
            } else {
                $query->condition($entity_type->getKey('bundle'), $configuration['target_bundles'], 'IN');
            }
        }

        if (isset($match) && $field = $configuration['target_field']) {
            $query->condition($field, $match, $match_operator);
        }

        // Add entity-access tag.
        $query->addTag($target_type . '_access');

        // Add the Selection handler for system_query_entity_reference_alter().
        $query->addTag('entity_reference');
        $query->addMetaData('entity_reference_selection_handler', $this);

        // Add the sort option.
        if ($configuration['sort']['field'] !== '_none') {
            $query->sort($configuration['sort']['field'], $configuration['sort']['direction']);
        }
        if (!$this->currentUser->hasPermission('bypass node access') && !count($this->moduleHandler->getImplementations('node_grants'))) {
            $query->condition('status', NodeInterface::PUBLISHED);
        }
        return $query;
    }
}
