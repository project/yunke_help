<?php
/**
 *  by:yunke
 *  email:phpworld@qq.com
 *  time:20180623
 */

namespace Drupal\yunke_help\Controller;


/**
 *
 * 通过类全限定名找到类文件路径
 *
 * @package Drupal\yunke_help\Controller
 */
class ClassFullPath
{
    /**
     * 根据类的完全限定名字空间类名查找类定义文件
     *
     */
    public function getClassFullPath()
    {
        return  \Drupal::formBuilder()->getForm("\Drupal\yunke_help\Form\ClassPath");
    }
}

