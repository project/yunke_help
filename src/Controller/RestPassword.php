<?php
/**
 *  by:yunke
 *  email:phpworld@qq.com
 *  time:20190812
 */

namespace Drupal\yunke_help\Controller;


/**
 * 用于在安装测试drupal时，解决由于时间过久忘记用户名和密码问题
 *
 */
class restPassword
{

    public function rest()
    {
        $user = \Drupal::currentUser();
        if ($user->isAnonymous()) {
            return ['#markup' => '匿名用户没有密码，无需重置'];
        }
        if (!$user->hasPermission('yunke help')) {
            return ['#markup' => '你需要有yunke help模块使用权限才能重置密码'];
        }
        return \Drupal::formBuilder()->getForm("\Drupal\yunke_help\Form\RestPassword");
    }

}

