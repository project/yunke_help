<?php

/**
 * 构建表单，重置密码
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class RestPassword extends FormBase
{

    public function getFormId()
    {
        return 'yunke_help_RestPassword';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['#title'] = '重置本账号密码';
        $form['password'] = array(
            '#type'     => 'textfield',
            '#title'    => "请输入新密码（无需验证原密码，首尾不包含空白字符）：",
            '#size'     => 100,
            '#required' => TRUE,
        );
        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type'        => 'submit',
            '#value'       => "提交",
            '#button_type' => 'primary',
        );
        $form['actions']['reset'] = array(
            '#type'       => 'html_tag',
            '#tag'        => 'input',
            '#attributes' => ['type' => 'reset', 'value' => "重填", 'class' => "button"],
        );
        $form['#attributes']["target"] = "_blank";
        return $form;
    }

    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        $password = $form_state->getValue('password');
        if (empty(trim($password))) {
            $form_state->setErrorByName('password', "密码不能为空");
        }
    }

    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        $newPassword = $form_state->getValue('password');
        $newPassword = trim($newPassword);
        $uid = \Drupal::currentUser()->id();
        $userEntity = \Drupal::entityTypeManager()->getStorage('user')->load($uid);
        $user = $userEntity->getAccountName();
        $userEntity->setPassword($newPassword)->save();

        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . "   \n\n";
        echo "用户名:" . $user . " \n密码已经被重置为：" . $newPassword;
        echo "\n\n";
        echo "\nby:yunke_help模块\n";
        echo "</pre>\n";
        die;
    }

}
