<?php

/**
 * 根据url查看路由结果
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class RouteByPath extends FormBase
{

    public function getFormId()
    {
        return 'yunke_help_route_by_path';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['#title'] = '显示路由系统处理结果';
        $form['path'] = array(
            '#type'  => 'textfield',
            '#title' => "输入路径（可从浏览器地址栏直接复制）：",
            '#size'  => 100,
            '#required' => TRUE,
        );
        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type'        => 'submit',
            '#value'       => "提交",
            '#button_type' => 'primary',
        );
        $form['actions']['reset'] = array(
            '#type'       => 'html_tag',
            '#tag'        => 'input',
            '#attributes' => ['type' => 'reset', 'value' => "重置", 'class' => "button"],
        );
        $form['#attributes']["target"] = "_blank";
        $form['#attached']['library'][] = 'yunke_help/removeFormSingleSubmit';
        return $form;
    }

    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        $path = $form_state->getValue('path');
        if (empty(trim($path))) {
            $form_state->setErrorByName('path', "路径不能为空");
        }
    }

    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        $path = $form_state->getValue('path');
        try {
            $route = \Drupal::service('router')->match($path);
            foreach ($route as $key => &$value) {
                if (in_array($key, ['_controller', '_raw_variables', '_access_result', '_route_object', '_route'])) {
                    continue;
                }
                if (is_object($value)) {
                    $value = "object";
                }
                if (is_array($value)) {
                    $value = "array";
                }
            }
        } catch (\Exception $e) {
            $route = "异常：找不到路由 可能原因：404页面、文件路径、数据库错误等";
        }

        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
        echo "当前指定的路径是：\n" . $path . "\n\n";
        echo "路由结果如下：\n";
        print_r($route);
        echo "\n</pre>";
        die;
    }

}
