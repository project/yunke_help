<?php

/**
 *  by:yunke
 *  email:phpworld@qq.com
 *  time:20180610
 */

namespace Drupal\yunke_help\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\EventSubscriber\RenderArrayNonHtmlSubscriber;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Access\AccessResult;

/**
 * 自定义控制器
 * 在研究drupal时，你可以用这个类来执行一些自定义控制器代码
 * 即使是在维护模式下，该控制器也可以访问的
 * Class Test
 *
 * @package Drupal\yunke_help\Controller
 */
class Test extends ControllerBase {


  public function __construct() {

  }


  /**
   * 提供自定义控制器代码运行
   *
   * @param Request $request
   *
   * @return array
   */
  public function test(Request $request) {
    echo "<pre>";
    echo "请先在以下位置自定义需要执行的PHP代码：\n";
    echo "文件：" . __FILE__ . "\n";
    echo "方法：" . __METHOD__ . "\n";
    die;

  }


  /**
   * 提供自定义控制器代码运行
   *
   * @param Request $request
   *
   * @return array
   */
  public function test_1(Request $request) {
    return [
      '#markup' => 'msg23',
    ];

    echo "<pre>";
    echo "请先在以下位置自定义需要执行的PHP代码：\n";
    echo "文件：" . __FILE__ . "\n";
    echo "方法：" . __METHOD__ . "\n";
    die;
  }

  /**
   * 提供自定义控制器代码运行
   *
   * @param Request $request
   *
   * @return array
   */
  public function test_2(Request $request) {
    echo "<pre>";
    echo "请先在以下位置自定义需要执行的PHP代码：\n";
    echo "文件：" . __FILE__ . "\n";
    echo "方法：" . __METHOD__ . "\n";
    die;

    $variables = [];
    $variables['#theme'] = "yunke_help_test";
    $variables['#title'] = "2号测试";
    $variables['#yunke_1'] = [
      '#markup' => "这是2号测试，可以传递三个变量：yunke_1、yunke_2、yunke_3",
    ];
    $variables['#yunke_2'] = "变量可以是任何类型，渲染数组可以直接打印，见云客源码分析系列的《twig服务》";
    $variables['#yunke_3'] = "本原始模板位于yunke_help/templates/yunke-help-test.html.twig";
    return $variables;

  }

  /**
   * 提供自定义控制器代码运行
   *
   * @param Request $request
   *
   * @return array
   */
  public function test_3(Request $request) {
    echo "<pre>";
    echo "请先在以下位置自定义需要执行的PHP代码：\n";
    echo "文件：" . __FILE__ . "\n";
    echo "方法：" . __METHOD__ . "\n";
    die;

    $variables = [];
    $variables['#theme'] = "yunke_help_test2";
    $variables['#title'] = "3号测试";
    $variables['#test_1'] = [
      '#markup' => "这是3号测试，可以传递三个变量：test_1、test_2、test_3",
    ];
    $variables['#test_2'] = "变量可以是任何类型，渲染数组可以直接打印，见云客源码分析系列的《twig服务》";
    $variables['#test_3'] = "本原始模板位于yunke_help/templates/yunke-help-test2.html.twig";

    $route_name = '<current>';
    $route_parameters = [];
    $options = [];
    $url = \Drupal\Core\Url::fromRoute($route_name, $route_parameters, $options);
    $variables['#test_3'] = $url;

    return $variables;
  }

}

