<?php
/**
 *  by:yunke
 *  email:phpworld@qq.com
 *  time:20180710
 */

namespace Drupal\yunke_help\Controller;


class Entity
{
    /**
     * 系统根目录的绝对路径 不带后缀斜杠 如：C:\root\drupal
     *
     * @var string
     */
    protected $root;

    /**
     * 本模块相对于系统根目录的路径，不带前后缀斜杠
     *
     * @var string
     */
    protected $yunke_help_path;


    public function __construct()
    {
        //$this->root = \Drupal::service("app.root");
        //$this->yunke_help_path = \Drupal::moduleHandler()->getModule("yunke_help")->getPath();
    }

    /**
     * 中转实体操作
     */
    public function index($method=null)
    {
        if(empty($method)){
            echo "链接不正确";die;
        }
        switch ($method){
            case "show-definitions":
                $this->showEntityTypeDefinitions();
                break;
            case "clear-definitions":
                $this->clearDefinitionsCache();
                break;
            default:
                echo "链接不正确";
        }
        die;

    }

    /**
     * 查看实体类型定义数据
     */
    public function showEntityTypeDefinitions()
    {
        $definitions=\Drupal::entityTypeManager()->getDefinitions();
        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
        echo "有如下实体类型：\n";
        print_r(array_keys($definitions));
        echo "\n定义如下：\n";
        print_r($definitions);
        echo "\n</pre>";
    }

    /**
     * 清理实体类型定义缓存
     */
    public function clearDefinitionsCache()
    {
        \Drupal::entityTypeManager()->clearCachedDefinitions();
        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . " BY:yunke_help模块\n\n";
        echo "已经清除全部实体类型定义数据的缓存\n";
        echo "\n</pre>";
    }

}

