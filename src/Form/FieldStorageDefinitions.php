<?php

/**
 * 显示实体类型储存字段定义
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\FieldableEntityInterface;


class FieldStorageDefinitions extends FormBase
{

    public function getFormId()
    {
        return 'yunke_help_Field_Storage_Definitions';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $entityTypeDefinitions = \Drupal::entityTypeManager()->getDefinitions();
        $entityTypeOptions = [];
        foreach ($entityTypeDefinitions as $id => $entityType) {
            if ($entityType->entityClassImplements(FieldableEntityInterface::class)) {
                $entityTypeOptions[$id] = $entityType->getLabel()."($id)";
            }
        }


        $form['description'] = [
            '#markup' => '选择一个实体类型，查看其储存字段定义，这会包含该实体类型的所有bundle的字段定义，系统以此确定数据库结构，该功能仅限可字段化实体'
        ];
        $form['entityType'] = [
            '#type'         => 'select',
            //'#title'        => '选择实体类型：',
            '#options'      => $entityTypeOptions,
            '#empty_option' => $this->t('-select-'),
            '#required'     => TRUE,
            '#field_prefix' => '选择实体类型：',
        ];
        $form['actions'] = [
            '#type' => 'actions',
        ];
        $form['actions']['submit'] = array(
            '#type'  => 'submit',
            '#value' => '查看',
        );
        $form['actions']['reset'] = [
            '#type'        => 'button',
            '#button_type' => 'reset',
            '#value'       => $this->t('Reset'),
            '#attributes'  => [
                'onclick' => 'this.form.reset(); return false;',
            ],
        ];
        $form['#attributes']['target'] = "_blank";
        $form['#attached']['library'][] = 'yunke_help/removeFormSingleSubmit';
        $form['#title'] = "显示实体类型的储存字段定义";

        return $form;
    }

    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        $entityType = $form_state->getValue('entityType');
        if (empty(trim($entityType))) {
            $form_state->setErrorByName('entityType', "请选择一个可字段化实体类型");
        }
    }

    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        $entityTypeId = $form_state->getValue('entityType');
        $entityFieldManager = \Drupal::service("entity_field.manager");
        $fieldStorageDefinitions = $entityFieldManager->getFieldStorageDefinitions($entityTypeId);


        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
        echo "当前指定的实体类型id是：" . $entityTypeId . "\n\n";
        echo "有如下储存字段定义（包含该实体类型的所有bundle字段定义）：\n";
        print_r(array_keys($fieldStorageDefinitions));
        echo "储存字段定义信息如下：\n";
        print_r($fieldStorageDefinitions);
        echo "\n</pre>";
        die;
    }


}
