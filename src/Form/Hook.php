<?php

/**
 * 根据钩子名查看钩子实现结果
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class Hook extends FormBase
{

    public function getFormId()
    {
        return 'yunke_help_hook_by_name';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['#title'] = '查看钩子实现';
        $form['hook'] = array(
            '#type'     => 'textfield',
            '#title'    => "输入钩子名：",
            '#size'     => 100,
            '#required' => TRUE,
        );
        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type'        => 'submit',
            '#value'       => "提交",
            '#button_type' => 'primary',
        );
        $form['actions']['reset'] = array(
            '#type'       => 'html_tag',
            '#tag'        => 'input',
            '#attributes' => ['type' => 'reset', 'value' => "重置", 'class' => "button"],
        );
        $form['#attributes']["target"] = "_blank";
        $form['#attached']['library'][] = 'yunke_help/removeFormSingleSubmit';
        return $form;
    }

    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        $hook = $form_state->getValue('hook');
        if (empty(trim($hook))) {
            $form_state->setErrorByName('hook', "钩子名不能为空");
        }
    }

    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        $hook = $form_state->getValue('hook');

        //得到普通钩子信息
        $implementations = \Drupal::moduleHandler()->getImplementations($hook);
        $hookInfo = "";
        if (empty($implementations)) {
            $hookInfo = "没有模块实现该钩子";
        } else {
            $hookInfo = "以下模块实现了该钩子，模块名及钩子函数名如下：\n";
            foreach ($implementations as $module) {
                $hookInfo .= $module . " : " . $module . '_' . $hook."();\n";
            }
        }

        //得到修改钩子信息
        $implementations = \Drupal::moduleHandler()->getImplementations($hook."_alter");
        $hookAlterInfo = "";
        if (empty($implementations)) {
            $hookAlterInfo = "没有模块实现该钩子的修改钩";
        } else {
            $hookAlterInfo = "以下模块实现了该钩子的修改钩，模块名及修改钩子函数名如下：\n";
            foreach ($implementations as $module) {
                $hookAlterInfo .= $module . " : " . $module . '_' . $hook."_alter();\n";
            }
        }

        //得到主题修改钩子信息
        $theme=\Drupal::theme()->getActiveTheme();
        $type=$hook;
        $theme_keys = [];
        foreach ($theme->getBaseThemeExtensions() as $base) {
            $theme_keys[] = $base->getName();
        }

        $theme_keys[] = $theme->getName();
        $functions = [];
        foreach ($theme_keys as $theme_key) {
            $function = $theme_key . '_' . $type . '_alter';
            if (function_exists($function)) {
                $functions[] = $function;
            }
        }
        $themeAlter='';
        if (empty($functions)) {
            $themeAlter = "没有主题实现该钩子的修改钩";
        } else {
            $themeAlter = "主题实现的该钩子修改钩函数名如下：\n";
            foreach ($functions as $function) {
                $themeAlter .= $function."();\n";
            }
        }

        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
        echo "注意：本功能不提供安装、卸载方面的钩子查询\n";
        echo "当前指定的钩子是：" . $hook . "\n\n";
        echo $hookInfo."\n\n";
        echo $hookAlterInfo."\n\n";
        echo $themeAlter."\n";
        echo "\n</pre>";
        die;
    }

}
