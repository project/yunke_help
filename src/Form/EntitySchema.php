<?php

/**
 * 显示实体类型共享表完整的数据库Schema
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\FieldableEntityInterface;


class EntitySchema extends FormBase
{

    public function getFormId()
    {
        return 'yunke_help_Entity_Schema';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $entityTypeDefinitions = \Drupal::entityTypeManager()->getDefinitions();
        $entityTypeOptions = [];
        foreach ($entityTypeDefinitions as $id => $entityType) {
            if ($entityType->entityClassImplements(FieldableEntityInterface::class)) {
                $entityTypeOptions[$id] = $entityType->getLabel()."($id)";
            }
        }
        foreach (array_keys($entityTypeOptions) as $id){
            $storage=\Drupal::entityTypeManager()->getStorage($id);
            if(!($storage instanceof \Drupal\Core\Entity\Sql\SqlEntityStorageInterface)){
                unset($entityTypeOptions[$id]);
            }
        }

        $form['description'] = [
            '#markup' => '选择一个实体类型，查看其共享表完整的Schema定义，使用该定义可以直接通过数据库组件提供的Schema处理器创建表，该功能仅限可字段化实体'
        ];
        $form['entityType'] = [
            '#type'         => 'select',
            //'#title'        => '选择实体类型：',
            '#options'      => $entityTypeOptions,
            '#empty_option' => $this->t('-select-'),
            '#required'     => TRUE,
            '#field_prefix' => '选择实体类型：',
        ];
        $form['actions'] = [
            '#type' => 'actions',
        ];
        $form['actions']['submit'] = array(
            '#type'  => 'submit',
            '#value' => '查看',
        );
        $form['actions']['reset'] = [
            '#type'        => 'button',
            '#button_type' => 'reset',
            '#value'       => $this->t('Reset'),
            '#attributes'  => [
                'onclick' => 'this.form.reset(); return false;',
            ],
        ];
        $form['#attributes']['target'] = "_blank";
        $form['#attached']['library'][] = 'yunke_help/removeFormSingleSubmit';
        $form['#title'] = "显示实体类型共享表Schema定义";

        return $form;
    }

    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        $entityType = $form_state->getValue('entityType');
        if (empty(trim($entityType))) {
            $form_state->setErrorByName('entityType', "请选择一个可字段化实体类型");
        }
    }

    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        $entityTypeId = $form_state->getValue('entityType');
        $storage = \Drupal::entityTypeManager()->getStorage($entityTypeId);
        $refStorage = new \ReflectionObject($storage);
        $methodName = 'getStorageSchema';
        if (!$refStorage->hasMethod($methodName)) {
            echo '该实体类型的储存处理器没有继承系统默认的储存处理器（Drupal\Core\Entity\Sql\SqlContentEntityStorage），不能处理';
            die;
        }
        $reflectionMethod_getStorageSchema = $refStorage->getMethod($methodName);
        $reflectionMethod_getStorageSchema->setAccessible(true);
        $storageSchema = $reflectionMethod_getStorageSchema->invoke($storage);
        $refStorageSchema = new \ReflectionObject($storageSchema);
        $reflectionMethod_getEntitySchema = $refStorageSchema->getMethod('getEntitySchema');
        $reflectionMethod_getEntitySchema->setAccessible(true);
        $schema = $reflectionMethod_getEntitySchema->invokeArgs($storageSchema, [$storage->getEntityType()]);

        /*
         * 额外提示：有了这个$schema后，可以通过以下代码创建实体类型的共享数据库表
        $schema_handler = \Drupal::database()->schema();
        foreach ($schema as $table_name => $table_schema) {
            if (!$schema_handler->tableExists($table_name)) {
                $schema_handler->createTable($table_name, $table_schema);
            }
        }
        */

        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
        echo "当前指定的实体类型id是：" . $entityTypeId . "\n\n";
        echo "有如下共享表：\n";
        print_r(array_keys($schema));
        echo "各共享表schema定义信息如下（该数组可以直接用于数据库组件建表，示例见本程序源代码）：\n";
        print_r($schema);
        echo "\n</pre>";
        die;
    }


}
